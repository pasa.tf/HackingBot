require 'sqlite3'
require 'active_record'

ActiveRecord::Base.establish_connection( adapter: 'sqlite3', database: 'bot.db' )

if !ActiveRecord::Base.connection.data_source_exists? 'relations' then
  class InitialSchema < ActiveRecord::Migration[5.0]
    def self.up
      create_table :relations do |t|
        t.references :f
        t.references :s
        t.references :t
        t.integer    :count
      end
      create_table :words do |t|
        t.string  :text
        t.boolean :begin, default: false
        t.integer :count
      end
    end

    def self.down
      drop_table :relations
      drop_table :words
    end
  end

  InitialSchema.migrate(:up)
end
