require 'sqlite3'
require 'active_record'

ActiveRecord::Base.establish_connection( adapter: 'sqlite3', database: 'bot.db' )

if !ActiveRecord::Base.connection.data_source_exists? 'chans' then
  class InitialSchema < ActiveRecord::Migration[5.0]
    def self.up
      create_table :chans do |t|
        t.boolean :blocked, true
        t.integer :uid, limit: 8
        t.string :last_sentence
        t.string :title
        t.timestamps
      end

      create_table :admins do |t|
        t.boolean :blocked, true
        t.integer :uid
        t.timestamps
      end
      add_index :chans, :uid, unique: true
    end

    def self.down
      drop_table :chans
    end
  end

  InitialSchema.migrate(:up)
end
