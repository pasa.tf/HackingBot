require 'sqlite3'
require 'active_record'
require_relative 'word_relations'
require_relative 'chan'

class Relation < ActiveRecord::Base
  belongs_to :f, class_name: :Word
  belongs_to :s, class_name: :Word
  belongs_to :t, class_name: :Word
  has_many :words
  def to_s
    "#{self.f} -> #{self.s} -> #{self.t}"
  end
end

class Word < ActiveRecord::Base
  belongs_to :relation
  def to_s
    self.text
  end
end

class Chan < ActiveRecord::Base
end

class Admin < ActiveRecord::Base
end
