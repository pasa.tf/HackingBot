require_relative "src/main"

# Convert hash to args
def get_arguments_hash
  easy_args = {}
  last_arg = "-option"
  ARGV.each do |arg|
    if arg.start_with?('-') then
      last_arg = arg
      easy_args[last_arg] = ""
    else
      if easy_args[last_arg].nil? || easy_args[last_arg] == "" then
        easy_args[last_arg] = arg
      else
        print "Erreur, vous avez rentré deux fois le même argument : #{last_arg}\n"
        exit 1
      end
    end
  end
  return easy_args
end

args = get_arguments_hash

if args["--import"] && args["-i"]
  import_file args["-i"], args["-o"]
elsif args["--import"] && args["-f"]
  Dir.open(args["-f"]).each_with_index do |file, index|
    folder = args["-f"]
    puts "#{file} #{index}"
    import_file "#{folder}/#{file}", args["-o"] if file != "." && file != ".."
  end
elsif args["--bot"]
  require_relative "src/bot"
elsif args["--talk"]
  if args["--talk"] != ""
    params = {}
    params[:f] =  args["--talk"]
    create_sentence params
  else
    create_sentence nil
  end
end