require 'net/http'
require 'pp'
require 'cgi'

$from = 3000;
$to = 4000;
$count_char = 0

($from..$to).each do |uid|
  p "#{((uid - $from) / ($to - $from).to_f * 100).round(2)}%"
  link = "https://danstonchat.com/#{uid}.html"
  uri = URI.parse(link);
  http = Net::HTTP.new(uri.host, uri.port)
  http.use_ssl = true
  response = http.get(uri.path)
  next if (response.code.to_i >= 400)
  body = response.body.force_encoding('UTF-8')
  sentences = body.scan(/<a *href="#{link}" *>((.|\n)+?)<\/a>/i)[0][0].scan(/\/span>((.|\n)+?)(<|$)/i)
  File.open('./dtc.txt', 'a:UTF-8') { |file| sentences.each { |s|
      text = CGI.unescapeHTML(s[0])
      if text.scan(/^([^ ^=]* ?){3}$/i) == []
        $count_char += text.length
        if $count_char > 4000
          p $count_char
          $count_char = text.length
          file.puts('')
          file.puts('========')
          file.puts('')
        end
        file.puts(text)
      end
    }
  }
end
