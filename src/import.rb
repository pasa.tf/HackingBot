require_relative 'main'



# Import to database
if (ARGV[0] != '--refactor' && ARGV.length == 1)
  File.open(ARGV[0], "r") do |b|
      while (line = b.gets)
        print line
        if(!!line.match(/^([^ ]+( |$)){3,12}$/) && !!line[0].match(/[a-z]/i))
          print ' imported'
          import(line, nil)
        end
        puts '--------------------'
      end
  end
# Refactor file to improve speed after
elsif (ARGV[0] == '--refactor' && ARGV.length == 3)
  File.open(ARGV[2], 'a') do |f|
    File.open(ARGV[1], "r") do |b|
        while (line = b.gets)
          if(!!line.match(/^([^ ]+( |$)){3,12}$/) && !!line[0].match(/[a-z]/i))
            p line
            f.puts(line)
          end
        end
    end
  end
end
