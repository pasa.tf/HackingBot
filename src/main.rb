require 'telegram/bot'
require_relative 'token'
require_relative '../db/database'
require 'sqlite3'
require 'active_record'

DEBUG = false

# Create new word
def word(text, first_sentence)
  _text = nil
  _text = text.downcase if !text.nil?
  w = Word.find_by text: _text
  if w.nil?
      w = Word.new
      w.text  = _text
      w.count = 0
      w.save
      puts "new Word : #{w.text}" if DEBUG
  end
  w.begin = true if first_sentence
  w.count += 1
  w.save
  return w
end

# Create relation from three words
def relation(f, s, t)
  r = Relation.find_by f: f, s: s, t: t
  if r.nil? && s
      puts "#{f.text} -> #{s.text} -> #{t.text}" if DEBUG
      r = Relation.new
      r.f = f
      r.s = s
      r.t = t
      r.count = 1
      r.save
  else
    r.count += 1
    r.save
  end
  return r
end

# Import text to database
def import(text, chan_uid)
  # Do not save message from blocked channels
  chan = Chan.find_by(uid: chan_uid)
  # return if chan_uid && (chan.nil? || chan.blocked || !line.match(/ /) || !line[0].match(/[a-z]/i))
  t = []
  text.split(/[.?:!\n]/m).each do |i|
    t.push(i.split(/[ ,-]/)) if !i.nil? && i != ''
  end
  t.map! do |tt|  tt.select do |ttt| ttt != "" end end
  print t if DEBUG

  t.each do |text_array|
    first_sentence = true
    text_array.each_with_index do |e, i|

      f = word(e, first_sentence)
      s = word(((i+1 > text_array.size) ? nil : text_array[i+1]), false)
      t = word(((i+2 > text_array.size) ? nil : text_array[i+2]), false)

      first_sentence = false
      relation f, s, t
    end
  end
end

def create_sentence params
  r = nil
  if (!params.nil? && params[:f])
    w = Word.find_by(text: params[:f])
    r = get_relation w, nil, nil
    return "Désolé, je ne peux pas accéder à votre requête" if r.nil?
  end
  "Article #{rand(1000)} :\n#{output r}"
end

def get_relation f, s, t
  a = nil
  if f && s && t
    a = Relation.where(f: f, s: s, t: t).order('count DESC').limit(20)
  elsif f && s
    a = Relation.where(f: f, s: s).order('count DESC').limit(20)
  else
    a = Relation.where(f: f).order('count DESC').limit(20)
  end
  a.offset(rand(a.count)).first
end

# Create new sentence from scratch
def output optional_relation

  max = 200
  word_count = 0
  message_test = 3
  message = ""
  i = 0

  while (message.split(" ").size < 3 || i < message_test)
    w = nil
    if (optional_relation)
      break if i > 0;
      i+=1
      r = optional_relation
    else
      words = Word.where(begin: true)
      w = words[Random.rand(words.count)]
      p words.count if DEBUG
      r = get_relation(w, nil, nil)
    end
      message = ""
    break if r.nil?
    message += r.f.text;

    while !r.f.text.nil?
      if word_count > max
        break
      end
      r = get_relation r.s, r.t, nil
      puts r if DEBUG

      break if r.nil?
      if r.f.text != ''
        message += " " + r.f.text
      else
        message += ", "
      end
      word_count+=1
      i+=1
    end
  end

  message.capitalize!
  # p message
  message = message + " " + r.s.text if !r.nil? && !r.s.text.nil?
  return message if message != ""

  # To not get empty message
  return output
end

# Add new Channel and update the last sentence and the title
def add_channel message
  chan = Chan.find_by uid: message.chat.id
  if chan.nil?
    chan = Chan.new
    chan.uid = message.chat.id
    chan.blocked = true
  end
  chan.title = message.chat.title
  chan.last_sentence = message.text.downcase
  chan.save
end

def toggle_channel(param)
  chan = Chan.find_by uid: param[:channel_id]
  return false if chan.nil?
  chan.blocked = param[:is_blocked]
  chan.save
  return true
end

def import_file from, out
  File.open(from, "r") do |b|
    while (line = b.gets)
      print line
      if out
        File.open(out, 'a') do |out_file| out_file.puts line end
      else
        import(line, nil)
      end
    end
  end
end

# def make_admin(uid)
#   if Admin.all.length == 0
#     new_admin = Admin.new
#     new_admin.uid = uid
#     new_admin.blocked = false
#     return
#   end


# end
