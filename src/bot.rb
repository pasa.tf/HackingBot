require_relative 'main'

# First time the app launch (to not spam when import all the conversation)
$uptime = Time.now

Telegram::Bot::Client.run(Token) do |bot|
  begin
      bot.listen do |message|
        begin
          if !message.text.nil?
            add_channel(message)
            msg = message.text.downcase
            p '-----------------------------'
            p msg
            case msg
            when /\/block -?[0-9]+/i
              uid = msg.scan(/^\/block (-?[0-9]+)/i).first.first
              toggle_channel(channel_id: uid, is_blocked: true)
            when /\/unblock -?[0-9]+/i
              uid = msg.scan(/^\/unblock (-?[0-9]+)/i).first.first
              toggle_channel(channel_id: uid, is_blocked: false)
            # when /\/admin -?[0-9]+/i
            #   make_admin(message.from.id)
            when /(^\/friend(@[a-z0-9]+)?|.*botaniste.*)/i
              requested_word = msg.scan(/\/friend@?[^ ]* ([^ ]+)? +([^ ]+)? ([^ ]+)?/i)
              bot.api.send_message(chat_id: message.chat.id, text: create_sentence(requested_word == [] ? nil: {f: requested_word[0][0], s: requested_word[0][1], t: requested_word[0][2]}))
            when /^\/debug/i
              p Word.all
            else
              chan = Chan.find_by(uid: message.chat.id)
              if !chan.nil? && !chan.blocked
                Thread.new {
                  cid = message.chat.id
                  text = msg
                  import(text, cid)
                  p "DONE" if text.length > 500
                }
                bot.api.send_message(chat_id: message.chat.id, text: output(nil)) if (rand(99) == 1 && (Time.now - $uptime > 60))
              end
            p '-----------------------------'
            end
          end
        end
      end
  rescue Telegram::Bot::Exceptions::ResponseError => error
    puts error
    puts error.backtrace
    retry
  end
end

